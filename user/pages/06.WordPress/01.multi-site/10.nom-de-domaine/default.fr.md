---
title: 'Étape 9 : les noms de domaine'
---

Comment configurer un nom de domaine personnalisé sur votre installation multisite ?
Après l’entrée et le plat de résistance, je vous propose de passer directement au dessert. J’espère que vous n’en ferez pas tout un fromage 😉

Plus sérieusement, vous allez maintenant découvrir, ou en savoir plus, sur le domain mapping, qui permet d’utiliser des noms de domaines différents pour les sites de votre réseau.

En termes de branding et de communication, cela peut s’avérer utile dans certains cas.

Reprenons à zéro. Rappelez-vous : sur votre installation, vous avez fait le choix – si possible – entre un sous-domaine et un sous-répertoire.

Avec un sous-domaine, votre site sera accessible sur une URL de ce type : https://votresupersite.votresite1.fr

Avec un sous-répertoire, votre site sera accessible sur une URL de ce style : https://votresupersite.fr/votresite1

Avec le domain mapping, vous allez pouvoir adopter une URL personnalisée.

Par exemple, votre site 1 s’appellera votresupersite.fr, et votre site 2 votre maginifiquesite.fr.

Finis les sous-domaines et les sous-répertoires, donc.

Information : Depuis la version 4.5 de WordPress, le domain mapping est supporté nativement par le CMS (Content Management System, Système de Gestion de Contenus). Vous n’avez plus besoin d’utiliser une extension pour configurer tout cela (sauf si vous utilisez toujours une version inférieure à la 4.5 : dans ce cas, mettez-vous à jour).

Pour commencer, vous devrez configurer vos zones DNS, notamment si vous avez acheté votre nom de domaine ailleurs que chez votre hébergeur.

Le Codex précise en effet que les noms de domaine doivent pointer à la racine du dossier de votre installation.

Si vous utilisez le HTTPS, assurez-vous que chaque nom de domaine dispose d’un certificat SSL.

Pour la marche à suivre concernant ces deux points (configuration DNS et certificat SSL), rapprochez-vous de votre hébergeur.

Ensuite, dirigez-vous vers le Tableau de bord de votre site et sélectionnez le site à modifier (Mes sites > Admin du réseau > Sites).

Dans mon cas, je vais attribuer un nom de domaine personnalisé à localhost/multisite/site1.

Modifier le nom de domaine d'un site
Dans le champ Adresse web du site (URL), renseignez le nom de domaine de votre choix (que vous devez évidemment posséder) et n’oubliez pas d’enregistrer les modifications.

Adresse web du site d'un domaine
Pour terminer, si une erreur se produit au moment de vous connecter à votre réseau de sites, les cookies sont sûrement en cause.

Ajoutez alors le morceau de code suivant dans votre fichier wp-config.php :

define('COOKIE_DOMAIN', $_SERVER['HTTP_HOST'] );

Code en cas de bug des cookies
Opération domain mapping terminée. Je vous parlais d’erreur juste avant. Je vous propose de poursuivre sur le sujet dans la dernière partie de cet article.

En guise de digestif, je vais vous parler du débogage d’une installation multisite.

Comment déboguer votre réseau ?
Comme vous avez pu le constater, paramétrer une installation multisite sur WordPress requiert un minimum de connaissances techniques.

Pour mener à bien votre mission, vous allez devoir un peu bidouiller le code.

Et qui dit code, dit erreurs possibles, avec un site qui devient inaccessible.

Homme dit Oh non
Pas de panique, tout cela peut s’arranger.

Pour commencer, vérifiez que vous n’avez pas oublié un chevron, une parenthèse ou un quelconque caractère dans les morceaux de code que vous avez ajoutés à vos fichiers wp-config.php et .htaccess.

Ensuite, les erreurs potentielles peuvent être multiples :

Une erreur de connexion à la base de données.
Une configuration des sous-domaines qui fait des siennes.
L’impossibilité pour les administrateurs de se connecter à leur site.
Une erreur 500 (Écran Blanc de la Mort).
Pour les résoudre, vous pouvez consulter notre guide des erreurs WordPress, la documentation du Codex, ou encore vous rapprocher de votre hébergeur (notamment pour la configuration des sous-domaines).

Bonus : quelques extensions incontournables
Quels plugins utiliser sur un WordPress multisite ? Si certaines extensions ne sont pas compatibles avec une telle installation, comme je vous l’ai expliqué, il y en aussi quelques-unes qui vont vous faciliter la vie au quotidien, parmi lesquelles :

User Switching permet de changer les utilisateurs de votre site WordPress, et de vous connecter à n’importe quel compte sans avoir à vous déconnecter à chaque fois.
User Role Editor vous aide à personnaliser les permissions d’un rôle (ex : permettre à un contributeur d’envoyer des images dans ses articles). Même si elle est un peu complexe à utiliser, elle fait bien le boulot.
Disable Comments vous donne la possibilité de désactiver les commentaires sur tout votre réseau de sites.
Multisite Enhancements : Ce plugin améliore la gestion du réseau pour les super-admin avec des fonctions utiles, comme la possibilité de montrer quel site a tel plugin actif, sur la page des plugins du réseau.
Multisite Toolbar Additions permet d’ajouter des liens dans le menu des super-admins et admins (ex : accès au réseau de thèmes et de plugins).
MultiSite Clone Duplicator vous aide à dupliquer n’importe quel site de votre réseau (fichiers, utilisateurs, rôles etc.).
