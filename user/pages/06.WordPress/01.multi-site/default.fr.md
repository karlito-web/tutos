---
title: 'Le Multi Sites : les  prérequis'
published: true
date: '2019-12-24 17:00'
publish_date: '2019-12-24 17:00'
visible: true
---

Première chose à savoir pour créer un réseau multisite sur WordPress : 

* vous devez obligatoirement être l’administrateur d’une installation WP.

* assurez-vous de disposer des outils suivants :

1. Un éditeur de code, pour modifier certains fichiers de WordPress. Parmi les plus célèbres, je pourrais vous citer Brackets et Sublime Text.
2. Un client FTP (File Transfer Protocol), c’est-à-dire un logiciel qui permet de communiquer avec votre serveur. Par exemple Filezilla, Cyberduck ou Transmit.
3. Un hébergeur web qui soit “compatible” avec les diverses fonctionnalités proposées par le multisite.

* installer WordPress.

Dans ce cas, j’ai LE [tuto](https://www.youtube.com/watch?v=7U1QovBFEoQ) qu’il vous faut : 

<iframe width="1280" height="720" src="https://www.youtube.com/embed/7U1QovBFEoQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Si vous comptez utiliser un site déjà en ligne et fonctionnel, pensez à effectuer une sauvegarde de vos fichiers et de votre base de données.

Enfin, le [Codex WordPress](https://codex.wordpress.org/?rel=nofollow&target=_blank) recommande également de désactiver vos extensions.