---
title: 'Étape 5 : ajouter un thème ?'
published: true
---

Afin d’ajouter un thème, vous devez vous rendre sur le Tableau de bord administrateur de votre réseau (Mes sites > Admin du réseau > Tableau de bord).

Par ailleurs, il est nécessaire que vous soyez Super-admin, ce qui sera automatiquement le cas si vous avez installé vous-même votre réseau de sites.

Dans votre barre latérale gauche, sélectionnez Thèmes > Ajouter.

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/themes-ajout.jpg)

Cherchez le thème de votre choix, par exemple GeneratePress. Cliquez sur Installer.

Une fois installé, WordPress va vous proposer d’activer le thème sur le réseau. Cliquez sur le bouton correspondant.

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/activer-reseau-theme.jpg)

Activer sur le réseau n’active pas votre thème sur chaque site, vous devrez ensuite procéder à l’activation individuellement.

En revanche, pour qu’un thème puisse être activé sur un site en particulier, il devra automatiquement être actif sur le réseau.

Pour en faire le thème par défaut sur le site de votre choix, vous devrez ensuite vous rendre sur le Tableau de bord du site en question, et activer le thème. Comme vous le feriez sur une installation WordPress classique.

Voici la marche à suivre avec mon site appelé Site numéro un. Je commence par me rendre sur son Tableau de bord (Mes sites > Site numéro un > Tableau de bord).

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/tableau-bord-site-un.jpg)

Puis rendez-vous dans Apparence > Thèmes. Logiquement, vous ne pourrez activer que des thèmes qui ont été au préalable activés sur le réseau.

Sélectionnez le thème de votre choix (ex : Twenty Nineteen) puis activez-le. Et c’est tout.

Enfin, il est aussi possible de désactiver des thèmes actifs sur le réseau pour un site en particulier.

Par exemple, si vous voulez être certain qu’il ne soit pas utilisé sur un site en particulier.

Ou au contraire, si vous voulez autoriser son activation sur tel ou tel site.

Pour cela, sélectionnez le site de votre choix sur votre réseau, puis cliquez sur Modifier.

Dans l’onglet Thèmes, vous pouvez activer/désactiver le thème de votre choix.

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/activer-desactiver-theme.jpg)

Pour l’exemple, j’ai choisi d’activer Twenty Sixteen. En me rendant dans Apparence > Thèmes du site en question (Site numéro un), vous allez voir que le thème est désormais prêt à être activer, si besoin.

Après les thèmes, place aux extensions. C’est l’objet de notre prochain développement.

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/exporter-twenty-sixteen.jpg)
