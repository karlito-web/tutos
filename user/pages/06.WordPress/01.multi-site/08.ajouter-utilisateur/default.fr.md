---
title: 'Étape 7 : ajouter des utilisateurs'
---

Vous savez gérer les thèmes et les plugins. Il manque deux gros morceaux pour terminer la cuisson de votre plat de résistance : l’ajout de nouveaux utilisateurs, et la gestion des mises à jour.

Montez un peu le feu, on va cuisiner tout ça en commençant par les utilisateurs.

Pour en ajouter un, direction le Tableau de bord de votre réseau (Mes sites > Admin du réseau > Tableau de bord).
Puis cliquez sur Utilisateurs > Ajouter.

Entrez un identifiant, une adresse email, et terminez en cliquant sur Ajouter un utilisateur.

Si vous vous rendez dans Tous les utilisateurs, vous découvrirez la présence du nouvel utilisateur que vous venez de créer (pour l’exemple, je l’ai nommé superutilisateur1).

Dans l’encadré rouge sur la droite, vous avez un aperçu des sites auxquels l’utilisateur a accès.

En tant que super-admin du réseau, vous avez les pleins pouvoirs. Cela signifie que vous pouvez supprimer un utilisateur, mais aussi le modifier.

Par contre, contrairement à une installation WordPress “classique”, vous ne pourrez pas accorder n’importe quel rôle à un utilisateur. Par défaut, il est abonné (plus d’infos là-dessus sur cette page) sur votre réseau de sites.

Si vous voulez lui donner plus de pouvoir, vous pourrez uniquement lui fournir les privilèges de super-admin (mais attention, dans ce cas, il aura les pleins pouvoirs).

Dans l’encadré rouge sur la droite, vous avez un aperçu des sites auxquels l’utilisateur a accès.

En tant que super-admin du réseau, vous avez les pleins pouvoirs. Cela signifie que vous pouvez supprimer un utilisateur, mais aussi le modifier.

Par contre, contrairement à une installation WordPress “classique”, vous ne pourrez pas accorder n’importe quel rôle à un utilisateur. Par défaut, il est abonné (plus d’infos là-dessus sur cette page) sur votre réseau de sites.

Si vous voulez lui donner plus de pouvoir, vous pourrez uniquement lui fournir les privilèges de super-admin (mais attention, dans ce cas, il aura les pleins pouvoirs).

Privilèges super-admin WordPress
Il est par contre possible de donner le rôle de votre choix à un utilisateur sur un site en particulier.

Pour cela, direction la partie Utilisateurs du site de votre choix (ex : Site numéro un).

Cliquez sur Ajouter, puis entrez l’email ou l’identifiant de votre choix, et le rôle désiré.

Donner un rôle à un utilisateur de votre choix
Vous voilà avec un nouvel utilisateur ayant le rôle d’administrateur sur votre site appelé Site numéro un.

Le rôle du super-utilisateur1
Précision de taille : l’administrateur d’un site sur un réseau multisite n’aura pas les pleins pouvoirs, comme c’est le cas pour un administrateur d’un site WordPress “classique”.

Par exemple, il ne pourra pas installer de thèmes et d’extensions, ni les supprimer.

Le seul ayant la mainmise sur tous les aspects du réseau est le fameux super-admin.

Attention : Soyez vigilant lorsque vous allez accorder un rôle à vos nouveaux utilisateurs. Pour être bien au clair sur les privilèges accordés, je vous invite à consulter cet article d’Alex : Comment gérer les utilisateurs et les rôles dans WordPress.