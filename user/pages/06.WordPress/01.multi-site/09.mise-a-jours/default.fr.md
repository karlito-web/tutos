---
title: 'Étape 8 : les mises à jours'
---

Pour terminer notre tambouille, il nous manque un dernier élément : les mises à jour.

Comment ça se passe sur un réseau multisite ? De la même façon que sur installation WordPress qui n’utilise pas le multisite.

Dès qu’une mise à jour est disponible (de WordPress, d’un thème, d’une extension ou d’une traduction), vous serez notifié sur votre Tableau de bord, dans Mises à jour.

Mises à jour sur le Tableau de bord WordPress
Une fois la mise à jour lancée, elle s’appliquera sur tout votre réseau de sites, donc vous n’aurez pas à faire du cas par cas.

O.-K., vous pouvez baisser le feu. La cuisson de votre plat de résistance touche à sa fin.

Laissez encore un peu mijoter tout ça, juste le temps d’évoquer un sujet de taille : la sécurité de votre réseau multisite.

La sécurité de votre réseau WordPress multisite
Je vous en ai parlé au début de cet article : en cas de piratage de l’un des thèmes ou plugins d’un des sites de votre réseau, les autres installations ont de grandes chances d’être impactées.

Afin de vous prémunir, pensez à appliquer les bonnes pratiques suivantes :

Utiliser des mots de passe forts, que ce soit vous ou les utilisateurs de votre site.
Sauvegardez régulièrement votre site (fichiers + bases de données).
Mettez le à jour dès que possible.
Utilisez une extension globale de sécurité comme SecuPress ou IThemes Security.
Passez votre site au HTTPS, si ce n’est pas encore fait.
Concernant ce dernier point, vous allez avoir besoin d’un certificat SSL, qui permet d’assurer une connexion sécurisée entre le serveur web et le navigateur.

Il est possible de l’activer gratuitement chez la plupart des hébergeurs, comme o2switch ou OVH.

Si vous utilisez Let’s Encrypt, disponible chez o2switch, vous devrez activer un certificat SSL sur chaque site.

Sinon, il faudra passer par un certificat wildcard, qui permet d’activer le HTTPS sur tous les domaines et sous-domaines.
