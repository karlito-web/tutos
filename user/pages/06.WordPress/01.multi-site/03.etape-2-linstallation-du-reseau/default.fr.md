---
title: 'Étape 2 : l’installation du réseau.'
---

* Cliquez sur le lien Création du Réseau,

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/creer-reseau-sites.jpg)

* donner un titre à votre réseau, et de vérifier que votre adresse email soit correcte.

* cliquez sur Installer.