---
title: 'Étape 1 : activez la configuration dans wp-config.php'
published: true
---

* éditer votre fichier wp-config.php (à la racine de votre site, sur votre client FTP)

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/wpconfig-ftp.jpg)


* chercher la ligne de commentaire suivante : /* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */.


* ajouter en-dessous, ajoutez le morceau de code suivant :

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/code-multisite-wpconfig.jpg)

> define('WP_ALLOW_MULTISITE', true);


*  enregistrez votre fichier et rafraîchissez votre navigateur.

*  se connecter à l'admin, 

* menu Outils, un nouveau lien intitulé "Création du réseau" est activé.

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/multisite-wordpress-creation-reseau.jpg)

**le multisite est autorisé sur votre site.**
