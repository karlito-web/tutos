---
title: 'Étape 4 : ajouter de nouveaux sites'
published: true
---

Sur votre Tableau de bord, suivez le chemin suivant : Mes sites > Admin du réseau > Sites, et cliquez sur le bouton Ajouter.

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/ajouter-site-reseau-1360x477.jpg)

Vous allez ensuite devoir renseigner 4 champs :

* Adresse web du site (URL) : indiquez le nom du sous-domaine ou du sous-répertoire de votre choix.
* Titre du site.
* Langue du site.
* Adresse de contact de l’administrateur : soit votre adresse email (dans ce cas vous pourrez accéder au site depuis votre tableau de bord principal), soit l’adresse email de l’utilisateur de votre choix (dans ce cas, le site sera accessible via l’onglet Mes sites du Tableau de bord).

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/ajout-site-dashboard.jpg)

Et voilà, le tour est joué. Comme vous pouvez le constater, mon nouveau site, baptisé site1, est désormais accessible depuis le sous répertoire du même nom.

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/localsite-numeroun.jpg)

Note : Une fois votre site créé, votre répertoire wp-uploads, qui se trouve dans le répertoire wp-content, ajoutera automatiquement un répertoire sites.
Ce répertoire sites contiendra tous les sous-répertoires correspondants à chaque site de votre réseau. Chacun d’entre eux sera identifié par un ID (numéro). Par exemple, les fichiers du site1 que j’ai créé (ex : les médias), seront stockés dans le sous-répertoire 2. Si je décide de créer un nouveau site sur mon réseau, ses fichiers seront stockés dans le sous-répertoire 3.

Vous pouvez modifier certains aspects du site en question en cliquant sur Modifier dans le menu Tous les sites.

C’est ici que vous pourrez notamment ajouter/modifier un utilisateur, ou encore activer un autre thème (par défaut, le thème utilisé est le même que celui de votre site principal).

![](https://wpmarmite.com/wp-content/uploads/2019/06/modifier-site.gif)