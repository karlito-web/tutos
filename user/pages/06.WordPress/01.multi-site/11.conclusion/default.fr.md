---
title: Conclusion
---

Tout au long de cet article, vous avez appris à installer et à gérer une installation de plusieurs sites.

Si vous estimez que c’est la bonne solution pour votre projet, prenez quand même quelques précautions avant d’agir.

Sur un site déjà en production, n’oubliez pas, au préalable, d’effectuer une sauvegarde de l’ensemble de vos fichiers et de la base de données.

Et si vous avez le moindre doute, réfléchissez-bien : une fois votre réseau installé, il n’est pas simple de revenir à une installation standard individuelle.

Maintenant, c’est à vous de prendre la parole. Que pensez-vous du multisite ? Utilisez-vous cette fonctionnalité ?

Donnez-moi votre opinion dans les commentaires et n’hésitez pas à partager vos astuces et remarques afin d’en faire profiter les autres lecteurs.