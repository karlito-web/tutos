---
title: 'Étape 3 : activez  le réseau multisite'
published: true
---

Quatre étapes sont nécessaires pour définitivement activer votre réseau multisite.

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/creer-reseau-sites-wordpress.jpg)

1. sauvegardez vos fichiers wp-config.php et .htaccess existants (si vous venez d’installer WordPress, ce n’est pas la peine).  

3. ajoutez le premier morceau de code à votre fichier wp-config.php, au dessus de la ligne /* C’est tout, ne touchez pas à ce qui suit ! Bonne publication de contenus ! */.
![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/code-wpconfig-activer-reseau.jpg)

>  define('MULTISITE', true);    
> define('SUBDOMAIN_INSTALL', false);  
> define('DOMAIN_CURRENT_SITE', 'sieg.wordpress');  
> define('PATH_CURRENT_SITE', '/');  
> define('SITE_ID_CURRENT_SITE', 1);  
> define('BLOG_ID_CURRENT_SITE', 1);  
  
3. configurez votre fichier .htaccess en y ajoutant le second morceau de code présent sur votre Tableau de bord.
  
> RewriteEngine On   
> RewriteBase /    
> RewriteRule ^index\.php$ - [L]    
  
> \# add a trailing slash to /wp-admin  
> RewriteRule ^([_0-9a-zA-Z-]+/)?wp-admin$ $1wp-admin/ [R=301,L]   
  
> RewriteCond %{REQUEST_FILENAME} -f [OR]  
> RewriteCond %{REQUEST_FILENAME} -d   
> RewriteRule ^ - [L]    
> RewriteRule ^([_0-9a-zA-Z-]+/)?(wp-(content|admin|includes).*) $2 [L]   
> RewriteRule ^([_0-9a-zA-Z-]+/)?(.*\.php)$ $2 [L]   
> RewriteRule . index.php [L]    
  
  
4. se reconnecter à votre Tableau de bord afin que les changements soient bien pris en compte.

Tout s’est bien passé ? le multisite est désormais actif sur votre site !

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/mes-sites-wordpress.jpg)
  