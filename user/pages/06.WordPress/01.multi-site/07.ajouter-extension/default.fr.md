---
title: 'Étape 6 : ajouter des extensions'
---

Toutes les extensions s’installent depuis le Tableau de bord de l’administrateur du réseau.

Ensuite, vous avez deux possibilités pour les activer :

Soit sur tout le réseau.
Soit individuellement, sur le site de votre choix.
Allez, on commence par l’installation sur tout votre réseau multisite sur WordPress. Cela peut notamment être utile si vous vous voulez utiliser une extension sur tous vos sites.

Par exemple, Yoast SEO, pour gérer finement le référencement naturel de vos contenus.

Rendez-vous sur la page Extensions de votre Tableau de bord réseau (Mes sites > Admin du réseau > Extensions).

Cliquez sur Ajouter, puis installez Yoast SEO. Et n’oubliez de l’activer sur le réseau ;-).

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/installer-yoast.jpg)

Désormais, Yoast SEO est activée sur tous les sites de votre réseau. Vous ne pouvez pas la désactiver individuellement sur chaque site, il est simplement possible de la désactiver du réseau.

Vous devez vous demander comment activer un plugin uniquement sur un site, pour rendre cela plus personnalisable ?

Minute, j’y viens.

Commencez par installer l’extension de votre choix depuis votre Tableau de bord réseau, comme vous l’avez fait avec Yoast.

Pour l’exemple, amusons-nous avec Redirection, qui permet de créer des redirections 301 (redirection d’un utilisateur d’une URL A vers une URL B).

Mais attention, n’allez pas trop vite. Il y a une petite subtilité : cette fois, n’activez pas l’extension sur le réseau.

Ensuite, allez dans le menu Extensions du site de votre choix (chez moi, Site numéro un).

Redirection a fait son apparition, il ne vous reste plus qu’à l’activer. Bravo !

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/06/redirection-activer.jpg)

Note : Toutes les extensions du répertoire officiel ne fonctionneront pas forcément dans un réseau multisite. N’hésitez pas à bien lire les descriptifs de chaque plugin, voire à contacter les développeurs concernés pour plus de précisions.
